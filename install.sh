#!/bin/bash

set -e
set -x

# update system
apt-get update
apt-get -y upgrade

# install generic dependencies
apt-get install -y --no-install-recommends \
	ca-certificates \
	build-essential \
	postgresql \
	libtool \
	gnupg2 \
	curl \
	jq zip \
	git


# install Taler components
./install-gnunet.sh
./install-exchange.sh
./install-bank.sh
./install-merchant.sh
./install-wallet.sh

#pg_ctlcluster 12 main start
#su -c "createuser root --superuser" postgres
#pg_ctlcluster 12 main stop

# clean up for smaller image size
apt-get -y autoremove --purge
apt-get clean
rm -rf /var/lib/apt/lists/*
