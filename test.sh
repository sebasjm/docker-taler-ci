#!/usr/bin/env bash

set -e
set -x

# test presence of exchange # TODO why does -v return error?
[[ "$(taler-exchange-httpd -v)" =~ "taler-exchange-httpd v" ]]

# test presence of bank
taler-bank-manage -h >/dev/null

# test presence of merchant # TODO why does -v return error?
[[ "$(taler-merchant-httpd -v)" =~ "taler-merchant-httpd v" ]]

# test presence of wallet
taler-wallet-cli -v >/dev/null

echo "Success: All components present! \o/"
