#!/bin/bash

set -e
set -x

apt-get install -y --no-install-recommends \
	autoconf \
	automake \
	libpq-dev \
	autopoint \
	pkg-config \
	zlib1g-dev \
	libltdl-dev \
	libgcrypt-dev \
	libsodium-dev \
	libsqlite3-dev \
	libjansson-dev \
	libcurl4-nss-dev \
	libunistring-dev \
	libmicrohttpd-dev \
	texinfo

# git clone https://git.gnunet.org/gnunet/
# cd gnunet
# ./bootstrap
# ./configure --with-jansson --with-microhttpd
# make install

# cd ..
# rm -rf gnunet
