#!/bin/bash

set -e
set -x

curl -sS https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
echo "deb https://deb.nodesource.com/node_12.x bullseye main" | tee /etc/apt/sources.list.d/node.list

apt-get update
apt-get install -y --no-install-recommends \
	nodejs

# install pnpm before building
npm install -g pnpm

# git clone git://git.taler.net/wallet-core
# cd wallet-core
# ./bootstrap
# ./configure --prefix=/usr/local/
# make install

# cd ..
# rm -rf wallet-core ~/.cache ~/.pnpm-store /usr/local/share/.cache
