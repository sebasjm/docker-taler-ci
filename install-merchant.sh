#!/bin/bash

set -e
set -x

apt-get install -y --no-install-recommends \
	libqrencode-dev

# git clone git://git.taler.net/merchant
# cd merchant
# ./bootstrap || ./bootstrap # this sometimes fails at the first run
# ./configure
# make install

# cd ..
# rm -rf merchant
