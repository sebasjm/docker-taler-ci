#!/bin/bash

set -e
set -x

apt-get install -y --no-install-recommends \
	python3-dev \
	python3-pip \
	python3-jinja2 \
	python3-psycopg2 \
	libpcre3 \
	libpcre3-dev


# git clone git://git.taler.net/bank
# cd bank
# ./bootstrap
# ./configure --destination=global
# make install

# cd ..
# rm -rf bank
