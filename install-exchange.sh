#!/bin/bash

set -e
set -x

apt-get install -y --no-install-recommends \
	recutils

# git clone git://git.taler.net/exchange
# cd exchange
# ./bootstrap || ./bootstrap # this sometimes fails at the first run
# ./configure
# make install

# cd ..
# rm -rf exchange
